import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

window.baseUrl = 'http://192.168.55.12:3000/';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);