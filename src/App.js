import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Map from "./Components/MapComponent";
import DashboardTable from "./Components/DashboardTableComponent";
import CompareForm from "./Components/CompareForm";
import Details from "./Components/DetailsComponent";

const latestAvailableDate = '10/06/2020';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch(window.baseUrl + "covid19.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.records
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error while loading COVID-19 information: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading COVID-19 dashboard...</div>;
        } else {
            let latestData = [];
            let mincases = 0;
            let maxcases = 0;
            items.forEach((country) => {
                if (country.dateRep === latestAvailableDate) {
                    latestData.push(country);

                    if (mincases === null || mincases >= country.cases) {
                        mincases = country.cases;
                    }
                    if (maxcases === null || maxcases <= country.cases) {
                        maxcases = country.cases;
                    }
                }
            });
            return (
                <div className="container">
                    <div className="App">
                        <Router>
                            <Switch>
                                <Route name="details" path="/details/:name">
                                    <Details allData={items} />
                                </Route>
                                <Route path="/" render={({history}) => (
                                    <div>
                                        <h1>COVID-19 Dashboard</h1>
                                        <Map countries={latestData} mincases={mincases} maxcases={maxcases} history={history} />
                                        <DashboardTable countries={latestData}/>
                                        <CompareForm countries={latestData}></CompareForm>
                                    </div>
                                )} />
                            </Switch>
                        </Router>
                    </div>
                </div>
            );
        }
    }
}

export default App;
