import React from "react";
import {useRouteMatch} from "react-router-dom";
import { Link } from "react-router-dom";
import Chart from "react-apexcharts";

function Details(props) {

    let match = useRouteMatch();
    let name = match.params.name;

    let countryData = props.allData.filter((dataPoint) => dataPoint.countriesAndTerritories === name);

    let data = [
        {
            name: 'New cases',
            data : countryData.map((dataPoint) => dataPoint.cases).reverse().slice(Math.max(countryData.length - 100, 0))
        },
        {
            name: 'New deaths',
            data : countryData.map((dataPoint) => dataPoint.deaths).reverse().slice(Math.max(countryData.length - 100, 0))
        },
    ];
    let categories = countryData.map((dataPoint) => dataPoint.dateRep).reverse().slice(Math.max(countryData.length - 100, 0))

    let options = {
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                horizontal: false,
            },
        },
        dataLabels: {
            enabled: false
        },
        xaxis: {
            categories: categories,
        }
    };

    return <div>
        <h1>COVID-19 Dashboard</h1>
        <h3 className="text-muted">{name.replaceAll('_', ' ')}</h3>

        <h4>Data over time</h4>
        <Chart options={options} series={data} type="bar" />

        <Link to="/" className="btn btn-secondary">
            Go back
        </Link>
    </div>
}

export default Details;