import React from "react";

class Tooltip extends React.Component
{
    render()
    {
        // Do not display tooltip when no children are present
        if (this.props.children.length === 0){
            return <div></div>;
        }
        return (
            <div className="covid19-tooltip" style={{
                left: this.props.positionX,
                top: this.props.positionY
            }}>
                {this.props.children}
            </div>
        );
    }
}

export default Tooltip;