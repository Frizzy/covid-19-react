import React from "react";
import { scaleLinear } from "d3-scale";
import {
    ComposableMap,
    Geographies,
    Geography,
    Sphere,
    Graticule
} from "react-simple-maps";
import Tooltip from "./TooltipComponent";
import { useHistory } from "react-router-dom";

const geoUrl =
    "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

class Map extends React.Component
{
    constructor(props) {
        super(props)

        this.state = {
            tooltipContent: '',
            positionY: 0,
            positionX: 0
        };
    }

    render() {
        let colorScale = scaleLinear()
            .domain([this.props.mincases, this.props.maxcases])
            .range(["#ffedea", "#ff5233"]);

        let data = this.props.countries;
        let renderTooltip = null;
        let history = this.props.history;

        return (
            <div>
                <ComposableMap
                    projectionConfig={{
                        rotate: [-10, 0, 0],
                        scale: 147
                    }}
                >
                    <Sphere stroke="#E4E5E6" strokeWidth={0.5} />
                    <Graticule stroke="#E4E5E6" strokeWidth={0.5} />
                    {data.length > 0 && (
                        <Geographies
                            geography={geoUrl}>
                            {({ geographies }) =>
                                geographies.map(geo => {
                                    const d = data.find(s => s.countryterritoryCode === geo.properties.ISO_A3);
                                    return (
                                        <Geography
                                            key={geo.rsmKey}
                                            geography={geo}
                                            onClick={(event) => {
                                                if (d === undefined){
                                                    // Antartica has no data, skip the tooltip.
                                                    return;
                                                }
                                                history.push('/details/' + d.countriesAndTerritories);
                                            }}
                                            onMouseEnter={(event) => {
                                                if (d === undefined){
                                                    // Antartica has no data, skip the tooltip.
                                                    return;
                                                }
                                                const pageY = event.pageY;
                                                const pageX = event.pageX;
                                                renderTooltip = setTimeout(() => {
                                                    this.setState({
                                                        tooltipContent: <div className="tooltip-content">
                                                            <span className="country-name">{d.countriesAndTerritories.replaceAll('_', ' ')}</span>
                                                            <strong>New cases: </strong> {d.cases}<br />
                                                            <strong>New deaths: </strong> {d.deaths}
                                                        </div>,
                                                        positionY: pageY,
                                                        positionX: pageX
                                                    });
                                                }, 100);
                                            }}
                                            onMouseLeave={() => {
                                                this.setState({
                                                    tooltipContent: '',
                                                    positionY: null,
                                                    positionX: null
                                                });
                                                clearTimeout(renderTooltip);
                                            }}
                                            fill={d ? colorScale(d.cases) : "#F5F4F6"}
                                            data-tip='tooltip'
                                            >
                                        </Geography>
                                    );
                                })
                            }
                        </Geographies>
                    )}
                </ComposableMap>
                <Tooltip positionY={this.state.positionY} positionX={this.state.positionX}>{ this.state.tooltipContent }</Tooltip>
            </div>
        );
    }
}

export default Map;