import React from "react";

class CompareForm extends React.Component
{
    render()
    {
        let countryOptions = this.props.countries.map((country) =>
            <option value={country.countriesAndTerritories} key={country.countriesAndTerritories}>
                {country.countriesAndTerritories.replaceAll('_', ' ')}
            </option>
        );
        return (
            <div>
                <form className="mb-5 hidden">
                    <div className="form-row">
                        <div className="col">
                            <select className="form-control" name="country1">
                                {countryOptions}
                            </select>
                        </div>
                        <div className="col">
                            <select className="form-control" name="country2">
                                {countryOptions}
                            </select>
                        </div>
                        <div className="col">
                            <button className="btn btn-primary">
                                Compare
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default CompareForm;