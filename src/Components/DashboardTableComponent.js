import React from "react";
import { Link } from 'react-router-dom';

class DashboardTable extends React.Component
{
    render()
    {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>New cases</th>
                        <th>New deaths</th>
                        <th>View details</th>
                    </tr>
                </thead>
                <tbody>
                {this.props.countries.map((country) =>
                    <tr key={country.countriesAndTerritories}>
                        <td>{country.countriesAndTerritories.replaceAll('_', ' ')}</td>
                        <td>{country.cases}</td>
                        <td>{country.deaths}</td>
                        <td>
                            <Link to={{pathname: `/details/${country.countriesAndTerritories}`}}>
                                Details
                            </Link>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        );
    }
}

export default DashboardTable;